#include <iostream>
#include <string.h>
using namespace std;

int main(int argc, char* argv[])
{
    cout<<"\tUsage: calcular_bcc.exe <Texto del que calcular bcc>"<<endl;
    int bcc=0;
    if(argc>1){
        cout<<"\tTexto: "<<argv[1]<<endl;
        for(unsigned int i=0; i<strlen(argv[1]);i++){
            bcc=bcc^argv[1][i];
            //std::cout<<argv[1][i]<<std::endl;
        }
        bcc=bcc|0x20;
        std::cout<<"\tBCC: "<<bcc<<std::endl;
    }
    return 0;
}
